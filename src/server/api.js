const express = require('express');
const bodyParser = require('body-parser');
const {first, second} = require('../routes/index.js');
const service = express();

service.use(bodyParser.json());

service.get('/status', (req, res) => {
  console.log('Checking API....')
  res.status(200).send('Demo API OK!');
});

service.get('/demo', first);
service.get('/test', second);

exports = module.exports = service;
